import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckStockDto } from './create-checkStock.dto';

export class UpdateCheckStockDto extends PartialType(CreateCheckStockDto) {}
