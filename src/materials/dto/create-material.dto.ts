export class CreateMaterialDto {
  id: number;

  // image: string;
  in_date: string;
  name: string;

  price: number;
  qt_previous: number;
  quantity: number;
  min: number;
  use: number;
  status: string;
}
