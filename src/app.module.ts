import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Product } from './products/entities/product.entity';
import { Promotion } from './promotion/entities/promotion.entity';
import { PromotionsModule } from './promotion/promotions.module';
import { ProductsModule } from './products/products.module';
import { Material } from './materials/entities/material.entity';
import { MaterialsModule } from './materials/materials.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

import { Role } from './roles/entities/role.entity';
import { AuthModule } from './auth/auth.module';
import { User } from './users/entities/user.entity';
import { CheckStock } from './checkStock/entities/checkStock.entity';
import { StockDetail } from './checkStock/entities/stockDetail.entity';
import { CheckStockModule } from './checkStock/checkStock.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',

      logging: false,
      entities: [
        Promotion,
        Product,
        Material,
        User,
        Role,
        CheckStock,
        StockDetail,
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    ProductsModule,
    MaterialsModule,
    PromotionsModule,
    AuthModule,
    CheckStockModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
