import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Promotion } from './entities/promotion.entity';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionsRepository: Repository<Promotion>,
  ) {}

  create(CreatePromotion: CreatePromotionDto): Promise<Promotion> {
    return this.promotionsRepository.save(CreatePromotion);
  }

  findAll(): Promise<Promotion[]> {
    return this.promotionsRepository.find();
  }

  findOne(id: number): Promise<Promotion> {
    return this.promotionsRepository.findOneBy({ id: id });
  }

  async update(id: number, updatePromotion: UpdatePromotionDto) {
    await this.promotionsRepository.update(id, updatePromotion);
    const promotion = await this.promotionsRepository.findOneBy({ id });
    return promotion;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionsRepository.findOneBy({ id });
    return this.promotionsRepository.remove(deletePromotion);
  }
}
